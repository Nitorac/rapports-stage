# Nomenclature des rapports

Dans le dossier rapports, des sous-dossiers par tuteur académique, puis des fichiers indiquant la classe, l'année, la note et le pseudo. Exemple : `rapports/dubois_catherine/2A_2018_15_Phoko.pdf`. *(Si note inconnue, enchainer directement année et pseudo : 2A_2018_Phoko.pdf)*

## Ajout d'un rapport

Pour ajouter votre rapport : fork le projet, ajouter votre rapport dans le bon dossier, et demander une pull request ici.
Si vous ne savez pas faire, envoyez-moi votre rapport par un autre moyen et je l'ajouterai. Précisez votre tuteur académique à l'ENSIIE, votre note et l'année du stage.

# Les templates

templates/<pseudo> pour trouver le template de <pseudo>.
Il est conseillé de mettre un README.md explicatif, ainsi qu'un pdf pour montrer à quoi ressemble le rapport.