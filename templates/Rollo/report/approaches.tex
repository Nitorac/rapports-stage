\chapter{Different approaches}
My mission was at first to find the best way to automate not only the documentation, but also the code. The main problematic was the automation, so it didn't matter if the code was generated from the documentation, or vice versa. Nevertheless, a raw material is need to generate something out of it, so I had to find what was most appropriate and gave the most satisfactory result. I then found several methods of automating either process, each with its advantages. I finally chose the one that offered the least constraints and the most flexibility.

\section{Code from the documentation}
This is the most logical approach, one must write the specification before the implementation. Furthermore, OpenAPI files are powerful: it's possible to easily write a complex API architecture with lots of features. Being able to generate code from it could speed up the development time.

Swagger offers an online editor that allows, in addition to validating the OpenAPI document, to generate source code, and even, a whole Spring project from it. Swagger Codegen is the engine running the code validation and can output more than 40 different formats. In the same way as the entire Swagger ecosystem it is currently under active development.

After having it tested testing with a documentation from one of the Unite APIs, it happened that the generated result is accurate and producing the expected result. But as Mercateo is using a lot of custom code and annotations, it's mandatory to make changes in the generated result that can't be done automatically. Each time the API was modified, it would have been necessary to rewrite manually most of the code. For this reason this method was not adopted, and we switched to the other approach: generating the documentation from the source code.

\section{Documentation from the code}
\subsection{Swagger annotations}
This is the first method in this category that has been tested a while ago by Team Atlas, and is the most common way to document an API in Java. The principle is quite simple, the documentation process is entirely done through the addition of specific annotations to the classes. This has several advantages, writing simple documentation is fast and easy and the annotations names are quite self-explanatory. However, for building a more complex documentation, it's needed to have good knowledge on the OpenAPI format as the use of annotations follows its structure. Furthermore, when we want to be precise, we quickly have many annotations and that can make the code unreadable. One positive point that can also be found with Auto-Restdocs (cf. later) is the automation of the schemata generation, directly by adding a special annotation to a bean. This thus allows to automatically document the skeleton of the HTTP requests and there's no place for mistake. An example of Note API documentation using this method can be found below:

\begin{lstlisting}[language=java]
// Swagger annotations
@Path("/note")
@Api(value = "/note")
@Produces({MediaType.APPLICATION_JSON})

// Spring annotations
@RestController
@RequestMapping(path = "/note", produces = MediaTypes.HAL_JSON_VALUE)
public class NoteController {
  @GET
  @Path("/")
  @ApiOperation(value = "Gets all the recorded notes", 
  notes = "Returns notes", 
  response = Note[].class
  )

  @GetMapping()
  public Collection<Note> index() { ... }


  @GET
  @Path("/{noteId}")
  @ApiOperation(value = "Gets a Note", 
  notes = "Returns the requested note", 
  response = Note.class
  )
  @ApiResponses(value = {
  @ApiResponse(code = 404, message = "No Note matching requested id found")
  })

  @GetMapping("/{id}")
  ResponseEntity<NoteResource> get(@ApiParam(value = "The id of the Note to get") @PathParam("petId") long id) { ... }


  @DELETE
  @Path("/{noteId}")
  @ApiOperation(value = "Deletes a Note", 
  notes = "The note has been deleted"
  )
  @ApiResponses(value = {
  @ApiResponse(code = 404, message = "Deletion failed no such Note with requested id")
  })

  @DeleteMapping("/{id}")
  void delete(@ApiParam(value = "The id of the Note to delete") @PathVariable long id) { ... }

  ...
}
\end{lstlisting}

Swagger annotations were unfortunately not used by Team Atlas as they wanted to ensure that the documentation is matching the code. Since the annotations are quite independent of the code, even if they are part of it, you can never be sure that when a change in the code is made, updating the annotations was not forgotten. When it comes to maintain a separate documentation in parallel, it's then easier to write a separate OpenAPI file as it offers more flexibility and re-usability (through the \textsc{components} block). That's why it was better to find another solution.

\subsection{Auto-Restdocs}
Auto-Restdocs is an independent project that aims, as its name tends to show, to automate the documentation in a manner that developers do not need to write anything anymore. It's strong feature is to read the documentation from the Javadoc\footnote{Javadoc is a documentation generator created by Sun Microsystems for the Java language for generating documentation from Java source code -- Wikipedia}, so developers only have to document their code, and nothing else. I liked the idea behind Auto-Restdocs, but unfortunately its output format can't be changed. It only generates AsciiDoc\footnote{AsciiDoc is another plaintext documentation syntax.} files that can be converted to HTML. I really wanted to produce OpenAPI documentation as Swagger UI is a practical tool to visualize it, so I tried in different manners to convert AsciiDoc files. In the end I was loosing information in the process, as it is not intended to be done this way. I really liked the idea to be able to read Javadoc and using it, because at the moment the hardest part in the documentation process is to document the beans. If it is possible for each of them to centralize the documentation in their own class file, it will prevent duplication and add more to the automation, as the developer will only have to write it in one place.

In the example below the HTTP request \textsc{GET} on the endpoint \texttt{/\{id\}} will return an object of type \texttt{ItemResponse}, annotated with Javadoc:
\begin{lstlisting}[language=java]
/**
 * @param lang Item name is returned in the specified language.
 */
@RequestMapping("{id}")
public ItemResponse getItem(@PathVariable String id, @RequestParam(value="language", required=false, defaultValue="en") String lang) { ... }

static class ItemResponse {
  /**
   * Item unique ID.
   */
  @NotBlank
  private Integer id;

  /**
   * Item order number.
   */
  @Min(1)
  private Integer orderNumber;

  /**
   * Item name.
   */
  private String name;
}
\end{lstlisting}

Auto-Restdocs will then produce the following tables, which makes it clear for an end user. The example here is really simple, but it also works with more complex ones and is a real help.

\begin{center}
  \captionof{table}{Request parameters documentation}
  \begin{tabular}{|l|l|l|l|}
    \hline
    Parameter & Type & Optional & Description\\
    \hline
    language & String & true &
      \begin{tabular}{@{}l@{}}Item name is returned in the specified language.\\ Default value: "en".\\\end{tabular}\\
    \hline
  \end{tabular}

  \captionof{table}{Response elements documentation}
  \begin{tabular}{|l|l|l|l|}
    \hline
    Path & Type & Optional & Description\\
    \hline
    id & Integer & false & Unique item ID.\\
    orderNumber & Integer & true &
      \begin{tabular}{@{}l@{}}Item order number.\\ Must be at least 1.\\\end{tabular}\\
    name & String & true & Item name.\\
    \hline
  \end{tabular}
\end{center}


\subsection{Spring Restdocs}
When doing my researches in finding existing tools for my goal, I found Spring Restdocs. It's a Spring plugin designed exactly for what I sought: providing an API\footnote{In this context API is not referring to an HTTP API, but rather to the Restdocs library that provides classes and methods.} for writing documentation generation tools. By itself Restdocs is able to generate AsciiDoc files, but I started using it for the possibility to build myself an intermediate tool capable of using it, or to edit an existing one. Restdocs documentation is generated using the tests classes. When the tests are run to check the behavior of the API, we take advantage of it to create the documentation of the method handling the HTTP request currently tested.

\subsubsection{Restodcs-RAML}
Restdocs-RAML\footnote{\url{https://github.com/ePages-de/restdocs-raml}} is a project I found on Github\footnote{\url{https://www.github.com} is a platform for publishing and collaborating on projects.} that uses Spring Restdocs to generate documentation. The RAML format as a syntax similar to YAML, and that's the reason why this project caught my attention. I first thought of converting RAML files into OpenAPI YAML, but I realized that it was more accurate to rewrite the whole project in order to make it directly generate the format I wanted. Restdocs-RAML is the basis of what I wrote afterwards, so my work is very inspired by it.

This project contains two different parts to successfully produce the final output, and it is the same structure I used for the library I built:
\begin{description}
	\item[the library] which uses Spring Restdocs and offers classes that are called when writing tests;
	\item[a Gradle plugin] which goal is to build the RAML file once the tests are finished.
\end{description}
The library itself is written in Java, but the Gradle plugin was in Kotlin, so I had to learn this language to understand it and adapt it to my needs. There was no Maven plugin offered, and as Team Atlas don't use Gradle for its project, I had the choice either to migrate their projects to Gradle or to create a plugin for Maven. I chose the second solution, as I was able to reuse lot of code from the Gradle plugin.

\subsubsection{Writting my own library using Spring Restdocs}
By seeing the solutions that currently exist and since none of them offered exactly what I wanted, I decided to create my own library on the basis of Restdocs-RAML. This project offered a good understandable skelton that I could use to generate in the end documentation in the OpenAPI format. In the following part I will explain the structure of this project and how it works.