\chapter{Restdocs-OpenAPI}
\section{Presentation}
Restdocs-OpenAPI is the name of the project I created to generate Spring API documentation in the OpenAPI format. As I wrote previously, my inspiration source was Restdocs-RAML and I followed its project structure. My project uses as well Spring Restdocs, a test driven documentation generation library. This has many advantages: when programming in Java it's common to follow a test driven development; this means that every time a change is made to the code, tests are run with a set of input data and known results. If the tests are failing, we know that there must be a bug somewhere, or that the tests cases are outdated. In both cases there is something to fix. Generating documentation in this context seems accurate as it takes into account the smallest change in the code, and therefore guarantees the most up-to-date version possible. In addition, since as soon as a new version of the API is released, the code is sent to a build server for continuous integration (like Jenkins). It is in charge of running the tests itself before deployment, which means additionally generating documentation. This allows us to take advantage of the existing architecture and it avoids adding work to the developer.

Restdocs-OpenAPI contains several components carrying out different steps of the process (similar to the project on which it is based):
\begin{description}
  \item[The library] Used to write tests, and in charge of generating snippets for each tested method.
  \item[Build tools plugins] Plugin for both Gradle and Maven to aggregate the fragments once the tests are complete.
  \item[A Swagger UI project] A simple setup of the interface to show the versioning usage.
  \item[A Spring project] An example project showing the usage of Restdocs-OpenAPI.
\end{description}

\section{The documentation generation process}
I will explain here the steps of the generation process, starting from the tests and going to the OpenAPI file. The use of Restdocs requires the writing of tests for each HTTP method on each endpoint, and for each possible result. For instance, if a \textsc{GET} request on the endpoint \texttt{/note} can produce either a \texttt{200} or a \texttt{404}, two tests methods need to be written to ensure a coverage of the documentation. One test request will generate an HTTP response with status code \texttt{200}, and the other test will be in charge of generating an error leading to the \texttt{404}.

\bigskip
\begin{figure}[h]
  \centering
  \includegraphics[width=15cm]{restdocs_openapi_process}
  \caption{Documentation generation process}
\end{figure}
\medskip

In the tests the HTTP methods are faked using MockMvc (Restassured, a similar library, can also be used), and this is where Restdocs works: it adds to MockMvc the ability to retrieve request and response data and to generate a snippet file, given its template, and classes telling it how to fill the file. The template file is what's called a \textit{fragment file}, in YAML format, and containing data following part of the OpenAPI specification to document only the method currently tested. What Restdocs is calling is the library of Restdocs-OpenAPI which filters the HTTP exchange data, and the documentations it gets through Restdocs, and fills the template.

We have in the end a lot snippet (or fragment) files, as well as object schemata or examples. Alone, they are not very significant and have to be merged together. This is done after the tests are finished by the plugin for Maven or Gradle. For both of them their principle is the same: they browse the folder containing the generated fragment files and merge them together by grouping them by endpoints and adding meta-data following the OpenAPI specification. This results in the final OpenAPI YAML file that is ready to be deployed and visualized with a Swagger UI.

\subsection{The library}
The Java library is the part that required the most effort, as it uses the Restdocs library which is quite complete and therefore somewhat complicated to use. This is what is more likely to evolve, because this is where the documentation is filled and interpreted. As for the Gradle plugin which only reads and writes files in the OpenAPI, its changes will be minimal and will be limited to following changes in the standard. The library is directly used by developers so it's logical that it should evolve to meet their needs.

\subsubsection{Writing the tests}
Writing tests is similar to \ref{SpringProject:Tests}, but there are additional calls, including to the \texttt{.andDo()} method that calls two elements of the Restdocs-OpenAPI library: \texttt{openAPIResource} and \texttt{OpenAPIResourceSnippetParameters}. These two classes implement some Restdocs functions and are responsible for the creation of the snippets files. To add documentation, you must call the \textit{builder}'s methods and add particular documentations objects from the library, or directly from Restdocs. For instance the object \textit{fieldWithParam} is used to document an element of the JSON HTTP response body. As there are lots of optional fields, using these objects' methods, developers can increase the accuracy of the documentation. A commented test for the \textsc{GET} method on the endpoint \texttt{/note/\{noteId\}} is given below:

\begin{lstlisting}[language=java]
// This test queries for an existing Note
@Test
public void Test02_getOk() throws Exception {
  // Here is the request path, with a placeholder for the id (with the value 1 here).
  mockMvc.perform(get("/note/{noteId}", 1)
    // The Content-Type expected is "application/json".
    .accept(MediaTypes.HAL_JSON_VALUE))
    // We expect a 200.
    .andExpect(status().isOk())
    // The JSON object {"note": {"id": 1, "content": "some content"}} is expected in the response.
    .andExpect(jsonPath("$.note.id").value(1)
    .andExpect(jsonPath("$.note.content").value("some content")
    // Here is where we call the documentation with Restdocs. "note-get-ok" will be the path under "build/generated-snippets" which will contain all the snippets for this test.
    .andDo(document("note-get-ok",
      // We use the OpenAPIResourceSnippetParameters builder because it offers more methods than the class shipped with Restdocs.
      openAPIResource(OpenAPIResourceSnippetParameters.builder()
      // In order to refer to the _links right we need to fill a unique operationId for the methods.
      // This only needs to be done once; for example in these two tests we are documenting the GET method, so it s useless to call .operationId() in the two tests.
      .operationId("noteGet")
      // It s the same here, summary() gives a small summary (of course) of what the method is doing.
      .summary("Gets a Note")
      // This description is not global for the method, it explains what is the expected result in this test
      .statusDescription("Returns the requested note")
      // We can also document parameters (such as headers, path parameters and query parameters).
      // We refer here to the one at line 5.
      .pathParameters(parameterWithName("noteId").description("The id of the Note to get"))
      // Here we document the response body using JSON paths
      // We start with the note object
      .responseFields(fieldWithPath("note").description("The note object").type(JsonFieldType.OBJECT),
        // Then we can document its elements
        fieldWithPath("note.id").description("The id").type(JsonFieldType.NUMBER),
        fieldWithPath("note.content").description("the content").type(JsonFieldType.STRING))
      .build())));
}
\end{lstlisting}

Restdocs also raises by default tests failures when there is missing or incomplete documentation of certain elements, and similarly when there is documentation for nonexistent elements. All this tends to reduce the number of errors and to have a complete documentation.

\subsubsection{The snippet files}
For each test a corresponding snippet file is generated, containing the documentation of the request result, but also schemata and examples. The example is just a dump of the test data (the request and the response), whereas the schemata\footnote{See \ref{Note:JSONSchema}, for reminder, JSON schemata are a way of describing JSON objects structure in JSON.} are generated using the \texttt{fieldWithPath} object of the library. An idea to explore is to be able, as with Auto-Restdocs, to extract comments, but also schemata, from the Javadoc. However, I didn't have time to implement this possibility.

Here is the fragment corresponding to the test above, with the schema and the example included, as normally these are two separate files. This fragment defines the request for the status code \texttt{200}, so we see the need to merge the files, because for instance if we have to describe other behaviors resulting in different status codes or using optional query parameters, this information can't just be trivially appended, the file needs to be entirely rebuilt.

\begin{lstlisting}[language=yaml]
get:
  summary: Gets a Note
  operationId: noteGet
  parameters:
    - name: noteId
      in: path
      description: The id of the Note to get
      required: true
      schema:
        type: string
  responses:
    200:
      description: Returns the requested note
      content:
        application/hal+json:
          schema:
            type: object
            properties:
              note:
                description: The note object
                type: object
                properties:
                  id:
                    description: The id
                    type: number
                  content:
                    description: the content
                    type: string
          example:
            note:
              id: 1
              content: some content
\end{lstlisting}

\subsection{The plugins}
Once the test completed, all the snippets are created and need to be parsed and aggregated. In the example project, the following directory structure is created:

\medskip
\dirtree{%
.1 build/.
.2 generated-snippets/.
.3 index-listing/.
.4 index-listing-response.json.
.4 index-listing-schema-response.json.
.4 openapi-resource.yaml.
.3 note-delete-nok/.
.3 note-delete-ok/.
.3 note-get-nok/.
.3 note-get-ok/.
.3 note-listing/.
.3 note-post/.
}
\medskip

There is one sub-directory for each test, containing the YAML fragment and the schemata in JSON. The mission of the plugin is to merge them all into one single file.

Gradle and Maven are the most common build tools to compile Java projects, so I had to provide a plugin for each of them. The plugins have a close working principle, except their configuration file. Gradle uses \texttt{build.gradle}, which is in the Groovy format, whereas Maven has a \texttt{pom.xml} in the XML format. Both of them are in charge of adding meta-data in the final OpenAPI file, like the name of the API for instance, and this is done in the plugin configuration. The following values are configurable and enable to configure API information, or the plugin behavior:

\medskip
\begin{tabularx}{\linewidth}{|l|X|X|}
  \hline
  Parameter name & Description & Default value\\
  \hline
  openAPIVersion & The version of the OpenAPI standard to follow. For the time, the most up-to-date version is 3.0.1. & "3.0.1"\\
  infoVersion & The version of the documented API.& "0.1.0"\\
  infoTitle & The fancy title, or the name given to the API. & "API Documentation"\\
  infoDescription & The description of the documented API. & \textit{null}\\
  infoContactName & The name of the entity to contact for support. & \textit{null}\\
  infoContactEmail & The email of the entity contact. & \textit{null}\\
  infoContactUrl & The URL where the support is. & \textit{null}\\
  serverUrl & The location of the server where the API is deployed. & \textit{null}\\
  serverDescription & A description of the server. & \textit{null}\\
  outputDirectory & The name of the directory (under the build directory) that will hold the YAML file. & "openAPIDoc/"\\
  snippetsDirectory & The directory containing all the generated fragments from the tests, in the output directory. & "generated-snippets/"\\
  outputFileNamePrefix & The name of the generated YAML file, without the extension. & "api"\\
  \hline
\end{tabularx}
\medskip

In the project by which I was inspired, there was at first just a Gradle plugin. I had to modify it to read and output OpenAPI data, but I also wrote a Maven plugin. I wanted to reuse a maximum of code, so I put the common code in a separated jar, so in the future, creating a new plugin will only require to use the build automation tool specific API; the code in charge of aggregating the fragment should be usable as is.

\subsection{The UI}
Apart from the automation of documentation, one important element was its versioning. This proved to be surprisingly easy using the Swagger UI, as it was just to modify its configuration. Since it is what Team Atlas is currently using, this only need minor modifications to be on service.

Swagger allows to use a JSON configuration file where several APIs can be referenced by their URLs. We can then specify several OpenAPI file, corresponding to the different released versions, as if there were different APIs. We can imagine that our OpenAPI files are stored on a S3 Bucket, so to deploy a new version it would be sufficient to upload it to the Bucket and to modify accordingly the Swagger configuration. 

Following is the configuration file that generated the UI in the screenshot after.We can see that adding a new version is very easy and only requires two lines. Plus, as the configuration file is read each time the UI is fetched by the client, since it's generated client-side, we don't need a laborious deployment process. Updating the configuration file is enough, and it's even possible to store it on a Bucket as well.

\begin{lstlisting}[language=json,caption={Configuration of Swagger for multiple versions},captionpos=b]
{"urls": [
    {"name": "API TEST v1",
      "url": "http://localhost:8080/api_0.1.0.yaml"},
    {"name": "API TEST v2",
      "url": "http://localhost:8080/api_0.2.0.yaml"}
  ]}
\end{lstlisting}

\bigskip
\begin{figure}[h]
  \centering
  \includegraphics[width=15cm]{swagger_notes}
  \caption{The Swagger UI showing the Notes API with different versions}
\end{figure}
