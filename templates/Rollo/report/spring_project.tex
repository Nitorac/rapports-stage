\chapter{Writing a Spring project}
In order to try several ways of generating documentation an experimenting with different libraries or software for this purpose, I decided to create my own project with the Spring framework. It was meant to be simple so the testing of new libraries and their implementation would be fast, because the existing APIs of the Unite system are too complex for doing this. This project will at the end be used to demonstrate how to use the library I wrote, Restdocs-OpenAPI, as an example project in the repository.

\section{API description}
In order to create a simple project a good start is to design a simple API. I decided to create one whose purpose is to manage notes. A \texttt{Note} object is a simple object containing an id and a content. It is possible with the API under the \texttt{/note} endpoint to list all and query existing notes, to create notes, and delete some. I wanted to be able to test the HATEOAS links, so under the base path \texttt{/} a \texttt{GET} request queries links to the note endpoint.

Below is the representation of the API using a UML diagram. It is quite simple, but I was able to generate it quickly and automatically based on the OpenAPI file corresponding to this API. This shows the benefits of this specification, it makes it possible gaining time by automating tasks.
\medskip
\begin{figure}[h]
  \centering
  \includegraphics[width=15cm]{notes_api}
  \caption{The Note API class diagram}
\end{figure}

\section{Project}
\subsection{Overview}
The structure of a Spring project is quite easy and it is simply required to write a few classes:
\begin{description}
  \item[controllers] Annotated classes to tell the Spring framework how to handle HTTP requests.
  \item[\textit{beans}] Data objects served to the clients or used internally.
  \item[tests] Classes used to ensure the proper behavior of the service.
\end{description}

The only \textit{bean} in this project is the \texttt{Note} object which is not of much interest; this is a simple object containing an \texttt{id} and a \texttt{content}. However, to show how Spring works, we ought to focus on the controllers and the tests.

\subsection{The controllers}
A controller class has the role to tell the framework how to handle this or that request. This process works through annotations\footnote{Annotations are some kind of meta-data in the code that add additional information on a class, a method, a variable...}. Each controller is a different class and can be mapped to a specific path, for example \texttt{/note}. They contain methods that are each linked to one HTTP request (\textsc{GET}, \textsc{POST}, \textsc{DELETE}, ...) on a specific path. Their parameters are the input data from the request (what the client sent) and their return value is what the client will receive. Through annotations, it's easy to read HTTP headers as well as query parameters, or to deserialize\footnote{By definition JSON is a \textit{serialized} format, which means a program has to parse it to extract its data.} JSON.

Below is an excerpt of the \texttt{NoteController} class. Using annotations we attach this controller to the path \texttt{/note} and declare methods that will handle \textsc{GET} requests (\texttt{@GetMapping}) or \textsc{DELETE} requests (\texttt{@DeleteMapping}). It is possible in these annotations to fill in a sub path; the class method \texttt{get} will then be attached to the path \texttt{/note/\{noteId\}} where \texttt{\{noteId\}} is a placeholder for some value that will be retrieved in the argument variable \texttt{id} (thanks to \texttt{@PathVariable}).

\begin{lstlisting}[language=java]
@RestController
@RequestMapping(path = "/note", produces = MediaTypes.HAL_JSON_VALUE)
public class NoteController {
  /**
   * Get all the Notes recorded.
   * @return A collection of the recorded Notes is returned.
   */
  @GetMapping()
  public Collection<Note> index() { ... }

  /**
   * Get a Note by id.
   * @param id The unique identifier of the {@link Note} to get.
   * @return The matching Note is returned if existing.
   */
  @GetMapping("/{id}")
  ResponseEntity<NoteResource> get(@PathVariable long id) { ... }

  /**
   * Deletes a Note by id.
   * @param id ID of the note.
   */
  @DeleteMapping("/{id}")
  void delete(@PathVariable long id) { ... }
  ...
}

\end{lstlisting}

\subsection{The test classes}
One may think it is not relevant to speak about tests here, but on the contrary it's quite important as this is where the magic will happen thereafter. The purpose of the test classes is to assert the proper functioning of the system and this helps to remove a large part of the bugs. Given input data, we expect a determined behavior and that's this behavior that is checked in the tests. The Spring framework offers a simple method for faking HTTP requests and asserting their results: MockMvc. Using MockMvc is really easy; the following code is used to test the note controller and is pretty self-explanatory. The following three test methods are executed in order and assert consecutively that a \texttt{Note} object, whose content is \textit{some content} and identifier is \textit{42}, can be created, retrieved and deleted.

\label{SpringProject:Tests}
\begin{lstlisting}[language=java]
@SpringBootTest
public class NoteTest {
  static MockMvc mockMvc;

  // Object used to serialize objects in JSON strings
  @Autowired
  ObjectMapper objectMapper;

  // MockMvc setup
  @Before
  public void setUp() {
    mockMvc = MockMvcBuilders.build();
  }

  @Test
  public void Test_post() throws Exception {
    mockMvc.perform(post("/note").contentType(MediaTypes.HAL_JSON_VALUE)
        .content(objectMapper.writeValueAsString(new Note(42, "some content")))
        .andExpect(status().isCreated());
  }

  @Test
  public void Test_get() throws Exception {
    mockMvc.perform(get("/note/{noteId}", 42).accept(MediaTypes.HAL_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.note.id").value(42))
        .andExpect(jsonPath("$.note.content").value("some content"));
  }

  @Test
  public void Test_delete() throws Exception {
    mockMvc.perform(delete("/note/{noteId}", 42).accept(MediaTypes.HAL_JSON_VALUE))
        .andExpect(status().isNoContent());
  }
  ...
}
\end{lstlisting}

\subsection{Running it}
What makes Spring really easy to use is the speed with which you can create and run a project. With only a few classes we have a working API. For this project I used Gradle to build it, with the Spring plugin, which adds tasks to Gradle and enables to run the project with a single command: \texttt{gradle bootRun}. A standalone HTTP server is then launched and the API can be accessed locally on the port 8080 by default.