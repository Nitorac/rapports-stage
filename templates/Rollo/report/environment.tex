\chapter{Technical environment}
As the project requires the use of several underlying concepts and technologies, I think it's important to mention them here. The reminders in this section will be brief and limited to the concepts necessary to understand the rest of this report, in order to make it accessible.

\section{The Hyper Text Transfer Protocol}

\begin{wrapfigure}{r}{5.5cm}
  \includegraphics[width=5.5cm]{http_cycle}
  \caption{The client always have to query first, the server cannot perform on his own.}
\end{wrapfigure}

HTTP is the protocol used to transfer data over the web. It is part of the Internet protocol suite and defines commands and services used for transmitting web page data. It uses a server-client model and stateless request-response protocol; this means that the client always have to query the server to perform an action.

An HTTP server exposes resources (most often web pages, but in the case of an API it's data) with which the client can interact by using different methods. In the common usage the two most used request are \textbf{GET}, used to query a page from the server, and \textbf{POST}, used to transmit data to it.

The HTTP specification describes the structure of the request and the response. They can be broken down into different blocks.

\begin{minipage}{6in}
  \centering
  \raisebox{-0.5\height}{\includegraphics[width=0.4\textwidth]{http_request}}
  \hspace*{1cm}
  \raisebox{-0.5\height}{\includegraphics[width=0.4\textwidth]{http_response}}
\end{minipage}
\begin{description}
	\item[URL] The resource location on the server.
	\item[Method] Represents an operation to be performed on the resource.
	\item[Headers] Meta-information of the request or response.
	\item[Body] Request or response payload data.
	\item[Status code] Numeric code corresponding to the result of the processing of the request by the server.
\end{description}

\section{What is an HTTP REST API?}
\subsection{API}
API stands for Application Programming Interface. In the web field an API is commonly offered by a server and enables clients to communicate with it in a standardized way over HTTP. This no longer consist of transmitting web pages anymore, but only data. The client is most likely not to be a web browser\footnote{The user through the browser is not directly initiating queries to the server, but the user interface that is running in the browser does.}, but rather a user interface or a software. The data transiting is serialized sub-formed to gain machine readability and improve efficiency. JSON\footnote{JavaScript Object Notation --- \url{https://www.json.org/}} is the most widely-used data format for this purpose.

\subsection{REST}
Representational State Transfer (REST) is an architectural style designed to take advantage of the HTTP protocol. It tends to minimize the data in the body block of the HTTP exchange and tries to maximize the use of all methods and status codes to perform actions. In the past lots of APIs were using only GET and POST request and were adding details upon the request content in the body block of the HTTP protocol. The result was that there was no rule to describing these subsidiary information, and that is what corrects the REST standard.

A web REST API consists of several endpoints\footnote{Generally an HTTP URI (Uniform Resource Identifier); example: http://myserver.com/api/fruit makes it possible to interact with the fruit resource.} that each represent a remote resource to handle on the server. The interaction with these resources is done through the different HTTP methods, and the method used depends on the kind of the type of operation desired. There are commonly four well-known methods:
\begin{description}
	\item[GET] Requests to retrieve a resource.
	\item[POST] Requests to create a new resource.
	\item[PUT] Requests to update an existing resource.
	\item[DELETE] Requests to delete a resource.
\end{description}

Unlike the \textit{regular} usage of the HTTP to get full web pages, it is here more common to have empty response body, the status code giving enough information of what appends server-side. Some of the most common status codes are\footnote{\url{https://restfulapi.net/http-status-codes/}}:
\begin{center}
\begin{tabular}{|l|l|l|}
	\hline
	Status code & Reason phrase & Description\\
	\hline
	200 & OK & The requested action has been carried out.\\
	201 & Created & Indicates that a new entity has been created.\\
	204 & No Content & The request succeeded but didn't generate any HTTP body.\\
	401 & Unauthorized & The required authorization was not provided.\\
	403 & Forbidden & Permission to operate on the resource is not granted.\\
	404 & Not Found & Remote resource is nonexistent.\\
	\hline
\end{tabular}
\end{center}

\subsection{HATEOAS}
\begin{wrapfigure}{r}{8cm}
  \vspace{-\parskip}
  \centering
  \includegraphics[width=8cm]{hal}
  \caption{Abstract representation of a resource with HAL links.}
  \medskip
\end{wrapfigure}

Hypermedia As The Engine Of Application State (HATEOAS) is a usage constraint of the REST architecture. It tends to add dynamic links to other resources on all responses of the server. It is then possible to fully explore the API based only on these links. A client of such an API does not need to know beforehand the API structure or the endpoints, they can be discovered on-the-fly. The only required item is a simple fixed URL, where the first request will be done. As a result given a service that follows the HATEOAS guidelines, the structure of its back-end can be deeply changed without modifying the front-end. We can compare this behavior to the navigation of a human on a website: he only needs to know the address of the website and can explore all its pages using hyper-links.

One of the most common conventions for defining hypermedia links is the Hypertext Application Language (HAL). It tends to be simple and easily applicable, especially to JSON and XML resources. HAL is structured in such a way as to represent elements based on concept of resources and links. Resources consist of URI links which a target URI, as well as the name of the link (referred to as \textit{rel}).

Let's imagine for example that we have an API designed to manage notes. A note is a simple object with two attributes: a unique identifier and a content. If we perform a request to list the recorded notes, we could expect a response (in JSON) like the one below. Each note object is returned with links (in the \textit{\_links} attribute) that gives us hints on which operations are available on the objects.

For instance the operation \texttt{someAction} is available for the note with the id \texttt{1}, and this note object can be queried at the address \texttt{https://example.com/api/note/1} (the self-reference).

\begin{lstlisting}[language=json]
{
  "notes": [
    {
      "id": 1,
      "content": "Some content",
      "_links": {
        "self": {
          "href": "https://example.com/api/note/1"
        },
        "someAction": {
          "href": "https://example.com/api/note/1/doSomething"
        }
      }
    },
    {
      "id": 2,
      "content": "Some other content",
      "_links": {
        "self": {
          "href": "https://example.com/api/note/2"
        },
        "someOtherAction": {
          "href": "https://example.com/api/note/2/doSomethingElse"
        }
      }
    }
  ]
}
\end{lstlisting}
\bigskip

The Atlas Team I worked with was intending to migrate gradually to the HATEOAS architecture, so the documentation generation solution had to be able to handle hyper-links.

\subsection{A flexible architecture}
Separating the user-interface (the \textit{front-end}) from the computation system (the \textit{back-end}) enables the system to be modular and more flexible. It is then possible to easily build dynamic applications by making background requests to the server through its API, or to build a micro-services architecture. There is no need anymore to host the whole website on the same host, but it is possible to share the load between a machine swarm, hosting the most demanding services on the more powerful servers (or in the cloud, on more expensive hosts dedicated to high load).

Using an API-based architecture makes it also possible to create an easy inter-service communication. Some services are then capable of calling several other back-ends to perform operations on data.

Another significant advantage of this architecture is the ability of using the same back-end for different usages and thus saving development time and efforts. For instance if a company is offering a website along with a smart-phone application, both of them have a different implementation, but the same behavior, and can refer to the same back-end through its API.

For example, the Unite system has APIs for the following services:

\begin{multicols}{3}
  \begin{itemize}
    \item{login}
    \item{registration}
    \item{contacting}
    \item{account}
    \item{tenant administration}
    \item{company administration}
    \item{administration}
  \end{itemize}
\end{multicols}

\newpage

\section{The Spring Framework}
\begin{wrapfigure}{r}{0pt}
  \centering
  \raisebox{0pt}[\dimexpr\height-5\baselineskip\relax]{%
    \includegraphics[width=3cm]{spring_logo}%
  }%
\end{wrapfigure}

Many APIs constituting the Unite back-end are mainly developed with the Spring Framework. The Spring Framework is an open-source\footnote{A software development model that encourage collaboration by disclosing the source code to the public.} Java platform intended to make the development of applications easier, and especially APIs. It is the most popular Java framework for enterprise application development. It embodies some of the finest and most popular design patterns, helping developers avoid rolling their own.

\subsection{Structure}
Spring has a very modular structure and was built on top of the idea of \textit{dependency injection} and \textit{inversion of control}. In normal words, instead of having a bunch of classes creating each other and passing each other from one place to another, you have a bag of \textit{beans}. Each bean declares its dependencies and Spring container resolves this requirements by automatically wiring everything together.

\begin{figure}[h]
  \centering
  \includegraphics[width=15cm]{spring_overview}
  \caption{Spring Framework overview}
\end{figure}

\subsubsection{Dependency injection}
This concept is one of the key elements that defines Spring and make development easier. Dependency injection helps to keep the classes as independent as possible by letting the framework handling their dependencies. That enables to replace dependencies without changing the class that uses them. It also reduces the risk of having to change a class just because one of its dependencies changed.

\subsubsection{Inversion of control}
Inversion of Control, or IoC, and is a design pattern\footnote{A software design pattern is a general, reusable solution to a commonly occurring problem within a given context in software design.} in which the control flow\footnote{Control flow is the order in which individual statements, instructions or function calls of an imperative program are executed or evaluated.} of an application is no longer controlled by the application itself, but rather the framework. The IoC coordinates the application activity; the developers define code blocks using the framework API, without relation between them. We find this principle in particular in the \textit{controllers} of Spring projects.

\subsubsection{Aspect Oriented Programming}
The Aspect Oriented Programming is a paradigm\footnote{A paradigm is a classification of programming languages and solutions to answer problems programatically.} which purpose is to increase scalability. It aims to prevent code duplication (and therefore increase code re-usability) and separate code used for third-party purpose (for example for \textit{logging}) from the objects.

\subsubsection{Beans}
Objects created by the container are also called managed objects or \textit{beans}. Objects can be obtained by means of either dependency lookup or dependency injection and are managed by the Spring IoC container. A bean can be \textit{wired}, which means an object can ask for a bean and the framework will then follow some rules to give it the proper instance.

\subsection{Java and build automation tools}
Spring is a Java framework, which means it can be used by any language running on the JVM\footnote{The Java Virtual Machine is in charge of executing the byte code generated after the compilation of the source code.} such as Java, Kotlin or Scala. The canonical and easiest way to compile source code in these languages is to use build automation tools, and the most popular are Gradle and Maven. They have different roles in order to build the project correctly:
\begin{itemize}
  \item{Compiling the classes}
  \item{Resolving the dependencies (such as external libraries)}
  \item{Generating meta-data (for publication)}
  \item{Running custom tasks, triggered by different actions (custom tasks can be added through plugins)}
\end{itemize}

Gradle and Maven offer the possibility of using public repositories where libraries can be downloaded and added to a project. To import a library, it is simply needed to add in the settings file of the project the \textit{artifactId} and the \textit{groupId}\footnote{These identifiers are a way of uniquely naming projects and to refer to them. See \url{https://maven.apache.org/guides/mini/guide-naming-conventions.html}}, as well as the requested version of the wanted library. As the compilation starts, this library will then automatically be downloaded and added.

A convenient aspect of these tools is the ability to add plugins to perform actions at different times of the build process. For instance the Spring plugin makes it easy to write and launch the application. It starts by adding the required dependencies for the framework to work correctly, and it also adds new tasks\footnote{A \textit{task} for Gradle, or a \textit{goal} for Maven, are names given to a set of actions to perform. For instance \textit{clean} will trigger the removal of compiled files, and \textit{build} will start the compilation.}, such as \textit{bootRun}, that will run the project using the framework with the right configuration. It relieves the developer from writing a lot of configurations and disregards the build environment.

\section{The cloud architecture}
\begin{figure}[h]
  \centering
  \includegraphics[width=15cm]{cloud_architecture}
  \caption{Deployment process of an API}
\end{figure}

Unite uses the Amazon cloud service to host its APIs. The whole architecture consists of several micro-services, but their role and their interactions are quite simple. The deployment process of a new API version will be described here, illustrated by the following diagram. We can see the flow of the documentation generation in green, or at least the most likely way of doing it, since it has not been done yet.

Everything starts when a developer creates a new commit\footnote{A commit can be summarized as a new change in the code on the git repository.} on the release branch\footnote{The code on the git repository is separated into independent branches which allow independent modifications on the code.} of the git repository. This calls a hook which triggers the Jenkins\footnote{Jenkins is an open-source automation server that helps to automate the development process with continuous integration.} server to fetch this branch and run the build process. Once the build is done and the tests are finished, a manual approval is needed to ensure no overwriting is done with an existing version. The built Docker\footnote{Docker is a program that performs operating-system-level virtualization, also known as "containerization", which consists of bundling all resources and libraries to make a standalone environment for a software to run.} image is then pushed to an Amazon ECR\footnote{Amazon Elastic Container Registry is a Docker container registry that makes it easy for developers to store, manage, and deploy Docker container images in the Amazon cloud.} and some events are sent to trigger further mechanisms. Events are processed by lambdas\footnote{Lambdas are some sort of functions that can be used as micro-services.} and dispatched across services. Images are then started by the ECS\footnote{Amazon Elastic Container Service is a scalable container orchestration service.} on EC2\footnote{EC2 are cloud instances that run the containers. The ECS orchestrator decides which instances are running or not.} instances.

The documentation generation in green is a possible way of doing it. Since it is generated during tests run on Jenkins, it is possible to have a task in charge of pushing it on a S3 Bucket\footnote{Amazon S3 Buckets are file repositories accessible via HTTP or a specific API.}. After that, it would be necessary to update the configuration of the Swagger UI to record the new generated version and its location. Since Swagger automatically fetches the file once its configuration is read, no more action would be necessary to achieve the documentation deployment.

\section{Documentation}
The actual documentation of the several APIs of the Unite back-end are files stored in an Amazon S3 Bucket, which are retrieved on demand by a web-UI running Swagger. When a user wants to read a specific documentation, the corresponding file is parsed and the UI is built.
Swagger is an open-source project, and one of the first documentation framework designed to maintain a synchronization with the code. A documentation specification has been designed in order to respect Swagger's standards, and then became the OpenAPI specification as it gained popularity.

\subsection{The OpenAPI documentation format}
\subsubsection{Presentation}
The OpenAPI format is maintained by the OpenAPI Initiative which ``recognize[s] the immense value of standardizing on how REST APIs are described.'' It is meant to be totally independent of language, technology or framework, and OpenAPI documents are indifferently JSON or YAML\footnote{Stands for \textit{YAML Ain't Markup Language}, and is a serialization language that aims to be more human-readable than JSON.} files. The format supports both API-first or code-first approaches to defining, building and documenting APIs, notably thanks to a wide set of code generators. It's for instance possible to design an API by its documentation and then generate the code in the desired language using the right code generator.

\subsubsection{Specification}
An OpenAPI file consists of several nested blocks arranged logically. It is possible to graphically represent this format very simply; you can refer to map representation in appendix \ref{appendix:OpenAPIMap}. Blocks contain keywords and values, and a value can either be another block or an object (a string, a boolean, a number...). This keyword-object association enables to understandably describe the API. An example file is given in appendix \ref{appendix:OpenAPIFile}.

The most common blocks are as follows:
\begin{description}
  \item[info]{Contains API meta-data such as title or description.}
  \item[servers]{Lists the addresses of the servers serving the API.}
  \item[components]{Contains resources such as schemata or examples that can be referenced throughout the whole document.}
  \item[paths]{Block that maps endpoints of the API to a \texttt{path} block.}
  \item[path]{Describes a specific endpoint with several HTTP \texttt{methods}.}
  \item[method]{Description of an HTTP request, with \texttt{parameters}, request and response bodies, status codes.}
  \item[parameter]{Can be a HTTP header, a query parameter or a path parameter.\footnote{A path parameter is found in the path of the URI, such as \texttt{/some/\{parameter\}} whereas a query parameter is defined after the resource: \texttt{/resource?parameter=value}}}
  \item[schema]{Block in the JSON Schema\footnote{\label{Note:JSONSchema}JSON Schema is a vocabulary that allows you to annotate and validate JSON documents. -- \url{https://json-schema.org/}} format for describing the request or the response bodies.}
  \item[example]{Gives an example value of an object. The most common use is for the HTTP bodies.}
\end{description}

\subsection{The Swagger UI}
Swagger UI is the most popular tool to visualize such documentations. Plus, it's the most featured tool to use as the specification was created partly by Swagger. As it can be read on the git repository, ``Swagger UI is a collection of HTML, Javascript, and CSS assets that dynamically generate beautiful documentation from a Swagger-compliant API''\footnote{\url{https://github.com/swagger-api/swagger-ui}}. The UI is generated on the client side each time the page is requested, so this allows to simply store the documentation file on a file or HTTP server, without having to deploy a complex file serving architecture.

Here is an example of the documentation for a public API of the Unite system. The UI is interactive, clear and legible. Each endpoint is expandable and this shows the available methods to interact with it, as well as the status code and the content of the response.

\bigskip
\begin{figure}[h]
  \centering
  \includegraphics[width=17cm]{swaggerui}
  \caption{Truncated UI showing the Unite tenant API}
\end{figure}